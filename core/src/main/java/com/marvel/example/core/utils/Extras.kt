package com.marvel.example.core.utils

/**
 * Copyright (c) 2019, Kurt Renzo Acosta, All rights reserved.
 *
 * @author Kurt Renzo Acosta
 * @since 21/04/2019
 */
object Extras {
    object CharacterDetails {
        const val CHARACTER_ID = "1"
    }
}